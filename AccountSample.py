from ATM import Account

# Create account

acc = Account(name="Arockia",address="Mudichur",contact="9789862971")
print acc.show_account()

acc1 = Account(name="Senthil")

print acc1.show_account()

# Show_Balance

print acc.show_balance()

# Deposit some amount

print acc.deposit(100000)

# Show balance now

print acc.show_balance()

# Withdraw 20000

print acc.withdraw(20000)

# Show balance now

print acc.show_balance()
