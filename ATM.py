class Account:
    '''
    This is to represent ATM with OOPS Concept
    '''

    def __init__(self, name, address=None, contact=None):
        self.name = name
        self.address = address
        self.contact = contact
        self.balance = 0
        if ( address and contact ) is None:
            self.type = "Temporary"
        else:
            self.type = "Permananent"

    def deposit(self,amount):
        self.balance += amount
        return "Amount Deposited Successfully. Ledger balance Rs. {}".format(self.balance)

    def show_balance(self):
        return self.balance

    def withdraw(self, amount):
        if self.balance == 0:
            return "Sorry. You don't have sufficient balance... Ledger balance Rs. {}".format(self.balance)
        else:
            self.balance -= amount
            return "Amount Rs. {} has been withdrawn succesfully... Ledger balance Rs. {}".format(amount, self.balance)

    def show_account(self):
        return self.type