import os

# Variable Declaration
# Dynamic variable types

val1 = "FirstScript"
val2 = 30
val3 = ['1',2,[3,4]]
val4 = {"name":"arockia","designation":"Senior infra Dev"}
val5 = ("super","super1")

print "val is {} ".format(type(val1))
print "va2 is {} ".format(type(val2))
print "va3 is {} ".format(type(val3))
print "va4 is {} ".format(type(val4))
print "va5 is {} ".format(type(val5))

# Variable Operations

val1 = "SecondScript"
print val1
val2 = 40
print val2
print val2+20

val4["name"]="new_name"

print val4
# List Operations

val3.append(5)
val3.insert(len(val3),6)
val3.reverse()
print val3


# The above data types are mutable which means we can change the values of the variable reference


# Passing arbitary number of arguments to the function. Plain arguments and keyword arguments.
def test(name, *args):
    print name
    print args

def test1(name,*args,**kwargs):
    print name
    print type(args)
    print type(kwargs)


test("Arockia","arulnahtn","testvalue")
test1("Arocia","Arul","Nathan","test1",degree="MCA",new_var="Test1")