class Student:

    def __init__(self, name, classname, rollno, gf=None):
        self.name = name
        self.classname = classname
        self.rollno = rollno
        self.GF = gf

    def show_test(self):
        return "Test"

    def add_gf(self,gf=None):
        self.GF = gf

    def show_detail(self):
        print self.GF
        if self.GF is None:
            return "Student {} from {} with rollno {}".format(self.name, self.classname, self.rollno)
        else:
            return "Student {} from {} with rollno {}... Girl Friend name is {}".format(self.name, self.classname, self.rollno, self.GF)